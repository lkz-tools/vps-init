# VPS Init script

Initialize Debian server with required tools and packages.

## Usage

```bash
# Lastest version
$ curl https://gitlab.com/lkz-tools/vps-init/-/raw/main/install.sh?inline=false | bash

# Install from specific version
$ curl https://gitlab.com/lkz-tools/vps-init/-/raw/main/install.sh?inline=false | bash -s -- --release v2.0.0-snapshot

# Install from git branch
$ curl https://gitlab.com/lkz-tools/vps-init/-/raw/main/install.sh?inline=false | bash -s -- --branch feature/exemple

# launch init
$ init_vps
```

## Environment variables

VPS_UTILS_DIR: VPC utils scripts directory
VPS_UTILS_VERSION: VPC utils scripts version
