#!/bin/bash

GL_HOST="gitlab.com"
GL_PROJECT_ID="44640889"

# Check args

usage() {
    echo "Usage: $0 [--release <version>] || [--branch <branch>]"
    exit 1
}

if ! test $# != 0 -o $# != 2; then
    echo "Invalid number of arguments provided"
    usage
fi

if [ $# == 0 ]; then
    GL_FORCE_VERSION=""
    GL_BRANCH_NAME=""
elif [ $1 == "--release" ]; then
    if [ -z $2 ]; then
        echo "Release version cannot be empty"
        usage
    else
        GL_FORCE_VERSION=$2
    fi
elif [ $1 == "--branch" ]; then
    if [ -z $2 ]; then
        echo "Branch name cannot be empty"
        usage
    else
        GL_BRANCH_NAME=$2
    fi
else
    usage
fi

# Check if curl and jq are installed, if not, install them
if ! command -v curl >/dev/null 2>&1; then
    apt update && apt install -y curl
fi
if ! command -v jq >/dev/null 2>&1; then
    apt update && apt install -y jq
fi

# Get latest tag (excluding 'snapshot' tags)
if [ ! -z "${GL_FORCE_VERSION}" ]; then
    GL_TAG="${GL_FORCE_VERSION}"
elif [ -z "${GL_BRANCH_NAME}" ]; then
    GL_TAGS_RES=$(curl -s "https://${GL_HOST}/api/v4/projects/${GL_PROJECT_ID}/repository/tags")
    if [ $? -ne 0 ]; then
        echo "Failed to get latest tag. Tag might not exist or you might not have access to the project"
        exit 1
    fi
    GL_TAG=$(echo "${GL_TAGS_RES}" | jq -r '.[].name' | grep -v snapshot | head -n 1)
else
    GL_TAG="${GL_BRANCH_NAME}"
fi

# Download install script
INSTALL_DIR="/opt/vps-utils"
ENTRY_SCRIPT="vps_utils.sh"

mkdir -p "${INSTALL_DIR}"
echo "Downloading install script ${GL_TAG}..."
curl --fail --silent "https://gitlab.com/lkz-tools/vps-init/-/raw/${GL_TAG}/${ENTRY_SCRIPT}?inline=false" -o "${INSTALL_DIR}/${ENTRY_SCRIPT}"
if [ $? -ne 0 ]; then
    echo "Failed to download install script"
    exit 1
fi

# Add to /root/.bashrc if not already there
echo "Installing vpc-utils..."
if ! grep -q "${INSTALL_DIR}/${ENTRY_SCRIPT}" /root/.bashrc; then
    echo "source ${INSTALL_DIR}/${ENTRY_SCRIPT}" >>/root/.bashrc
fi
if ! grep -q "export VPS_UTILS_DIR" /root/.bashrc; then
    echo "export VPS_UTILS_DIR=\"${INSTALL_DIR}\"" >>/root/.bashrc
fi
if ! grep -q "export VPS_UTILS_VERSION" /root/.bashrc; then
    echo "export VPS_UTILS_VERSION=\"${GL_TAG}\"" >>/root/.bashrc
else
    sed -i "s/export VPS_UTILS_VERSION=.*/export VPS_UTILS_VERSION=\"${GL_TAG}\"/g" /root/.bashrc
fi

# Run install script
echo "Restart a new shell or run: "
echo "--------------------------------------"
echo "$ source ${INSTALL_DIR}/${ENTRY_SCRIPT}"
echo "--------------------------------------"
echo "to start using vps-utils, then run the following command to start using vps-utils:"
echo "--------------------------------------"
echo "$ vps_utils"
echo "--------------------------------------"
