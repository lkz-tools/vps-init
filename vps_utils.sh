#!/bin/bash

FILE_VERSION=$VPS_UTILS_DIR/.version
echo $VPS_UTILS_VERSION >$FILE_VERSION

FILE_STATUS=$VPS_UTILS_DIR/.status
if [ ! -f $FILE_STATUS ]; then
    echo "SYSOPS_BASE_INSTALLED=false" >$FILE_STATUS
    echo "NETOPS_BASE_INSTALLED=false" >>$FILE_STATUS
    echo "APP_BASE_INSTALLED=false" >>$FILE_STATUS
fi

# Parse FILE_STATUS to get the status of each module
IS_SYSOPS_BASE_INSTALLED="$(cat $FILE_STATUS | grep SYSOPS_BASE_INSTALLED | cut -d "=" -f2)"
IS_NETOPS_BASE_INSTALLED="$(cat $FILE_STATUS | grep NETOPS_BASE_INSTALLED | cut -d "=" -f2)"
IS_APP_BASE_INSTALLED="$(cat $FILE_STATUS | grep APP_BASE_INSTALLED | cut -d "=" -f2)"

# --- SYSTEM --- #

sysops_new_user() {
    echo -e "\n👤 Creating user"
    read -p "Enter username: " username
    adduser --home /home/$username $username
    usermod -aG sudo $username
    echo "✅ Done"
}
export -f sysops_new_user

sysops_pm() {
    echo -e "\n⚙️ Updating and Upgrading the system"
    apt update -y
    [[ $? == 0 ]] && echo "✅ Update Done" || echo "❌ Update Fail"
    apt upgrade -y
    [[ $? == 0 ]] && echo "✅ Upgrade Done" || echo "❌ Upgrade Fail"
}
export -f sysops_pm

sysops_base() {
    echo -e "\n📦 Installing the basic packages"
    apt install -y curl wget vim neovim htop net-tools dnsutils iptables iptables-persistent rsyslog
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    PROFILE_SH=/etc/profile.d/00-base.sh
    PROFILE_ALIASES_SH=/etc/profile.d/01-aliases.sh
    {
        echo 'apt --dry-run upgrade | grep -P "\d\K upgraded"'
    } >${PROFILE_SH}
    echo -e "\n🔗 Setting up aliases, colors and basic configuration"
    {
        echo "alias ll='ls -lrtF'"
        echo "alias la='ll -A'"
        echo "alias ..='cd ..'"
        echo "alias grep='grep --color=auto'"
        echo "alias df='df -h'"
        echo "alias du='du -h'"
        echo "alias top='htop'"
        echo "alias ifconfig='ip a'"
        echo "alias route='ip r'"
        echo "alias netstat='ss'"
        echo "alias apt='apt -y'"
        echo "alias logs='cd /LOGS'"
        echo "alias app='cd /DATA'"
        echo "alias vim='nvim'"
    } >${PROFILE_ALIASES_SH}

    echo "set mouse-=a" >>${APP_HOME_DIR}/.vimrc
    source ${APP_HOME_DIR}/.vimrc

    echo -e "\n⚙️ Setting up the system"
    read -p "Enter hostname: " hostname
    hostnamectl set-hostname $hostname
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"
    echo -e "127.0.0.1\t$(hostname)\n$(cat /etc/hosts)" >/etc/host

    echo -e "\n🔐 Setting up fail2ban"
    apt install -y fail2ban
    sed -i 's/bantime  = 10m/bantime  = 24h/g' /etc/fail2ban/jail.conf
    sed -i 's/findtime  = 10m/findtime  = 1h/g' /etc/fail2ban/jail.conf
    sed -i 's/maxretry = 5/maxretry = 10/g' /etc/fail2ban/jail.conf
    sed -i 's/destemail = root@localhost/destemail = alerts-$(hostname)@huugii.app/g' /etc/fail2ban/jail.conf
    sed -i 's/sender = root@<fq-hostname>/sender = noreply-fail2ban-$(hostname)@huugii.app/g' /etc/fail2ban/jail.conf

    echo -e "\n⚙️ Prepare Fail2Ban for Postgres"
    {
        echo "failregex = FATAL:  password authentication failed for user \"<F-ID/>\""
    } >/etc/fail2ban/filter.d/postgres-lockuser.conf
    {
        echo "[Definition]"
        echo "actionstart = if [ ! -z '<target>' ]; then touch <target>; fi; printf %%b \"<init>\n\" <to_target>; echo \"%(debug)s started\""
        echo "actionflush ="
        echo "actionstop ="
        echo "actioncheck ="
        echo "actionban = printf %%b \"+<ip> <matches> <fid>\n\" <to_target>; echo \"%(debug)s banned <ip> (family: <family>) m <matches> fid <fid>\"; /usr/local/bin/postgres_set_role_nologin <fid>"
        echo "actionunban = /usr/local/bin/postgres_set_role_login <fid>"
        echo ""
        echo "debug = [<name>] <actname> <target> <ip> <to_target> --"
        echo ""
        echo "[Init]"
        echo "init = 123"
        echo "target = /var/log/f2b/fail2ban.dummy"
        echo "to_target = >> <target>"
    } >/etc/fail2ban/action.d/postgres-action.conf
    {
        echo ""
        echo "[postgres-lockuser]"
        echo "enabled = false"
        echo "filter = postgres-lockuser"
        echo "action = postgres-action"
        echo "logpath = /var/lib/postgresql/data/log/postgresql*.log"
        echo "usedns = raw"
    } >>/etc/fail2ban/jail.conf

    echo -e "\n⚙️ Enabling Fail2Ban for SSH service"
    {
        echo "[sshd]"
        echo "enabled  = true"
        echo ""
    } >>/etc/fail2ban/jail.d/jail.$(hostname).conf

    systemctl restart fail2ban
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    echo -e "\n📰 Settings up system logs"
    echo -e "\t- Iptables"
    {
        echo ":msg,contains,\"ACTION=\" /var/log/iptables.log"
        echo "& stop"
    } >/etc/rsyslog.d/iptables.conf
    systemctl restart rsyslog

    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    sed -i 's/SYSOPS_BASE_INSTALLED=false/SYSOPS_BASE_INSTALLED=true/g' ${FILE_STATUS}
    IS_SYSOPS_BASE_INSTALLED=true
}

# --- NETWORK --- #

netops_fw_list_rule() {
    echo -e "\n🔥 Listing firewall rules"
    iptables --list --verbose --line-numbers
}
export -f netops_fw_list_rule

netops_fw_add_rule() {
    FW_CHAIN="$1"
    FW_RULE_TYPE="$2"
    FW_PROTOCOL="$3"
    FW_SOURCE_IP_PORT="$4"
    FW_DESTINATION_IP_PORT="$5"
    FW_STATE="$6"

    if [ -n "${FW_CHAIN}" ] && [ -n "${FW_RULE_TYPE}" ] && [ -n "${FW_PROTOCOL}" ] && [ -n "${FW_SOURCE_IP_PORT}" ] && [ -n "${FW_DESTINATION_IP_PORT}" ] && [ -n "${FW_CONNTRACK_STATE}" ]; then
        echo -e "\n🔥 Adding firewall rule"
    fi

    # chain
    if [ -z $FW_CHAIN ]; then
        echo "Select chain:"
        echo "1. INPUT"
        echo "2. OUTPUT"
        echo "3. FORWARD"
        read -p "Enter chain: " CHAIN
        case $CHAIN in
        1) FW_CHAIN="INPUT" ;;
        2) FW_CHAIN="OUTPUT" ;;
        3) FW_CHAIN="FORWARD" ;;
        *)
            echo "❌ Invalid chain"
            return
            ;;
        esac
    fi

    # type
    if [ -z $FW_RULE_TYPE ]; then
        echo "Select rule type:"
        echo "1. ACCEPT"
        echo "2. DROP"
        echo "3. REJECT"
        read -p "Enter rule type: " RULE_TYPE
        case $RULE_TYPE in
        1) FW_RULE_TYPE="ACCEPT" ;;
        2) FW_RULE_TYPE="DROP" ;;
        3) FW_RULE_TYPE="REJECT" ;;
        *)
            echo "❌ Invalid rule type"
            return
            ;;
        esac
    fi

    # protocol
    if [ -z $FW_PROTOCOL ]; then
        # all, tcp, udp, udplite, icmp, icmpv6,esp, ah, sctp, mh
        echo "Select protocol:"
        echo "1. TCP"
        echo "2. UDP"
        echo "3. UDPLITE"
        echo "4. ICMP"
        echo "5. ICMPv6"
        echo "6. ESP"
        echo "7. AH"
        echo "8. SCTP"
        echo "9. MH"
        read -p "Enter protocol (default: TCP) : " PROTOCOL
        case $PROTOCOL in
        1) FW_PROTOCOL="tcp" ;;
        2) FW_PROTOCOL="udp" ;;
        3) FW_PROTOCOL="udplite" ;;
        4) FW_PROTOCOL="icmp" ;;
        5) FW_PROTOCOL="icmpv6" ;;
        6) FW_PROTOCOL="esp" ;;
        7) FW_PROTOCOL="ah" ;;
        8) FW_PROTOCOL="sctp" ;;
        9) FW_PROTOCOL="mh" ;;
        10) FW_PROTOCOL="all" ;;
        *)
            if [ -z $PROTOCOL ]; then
                FW_PROTOCOL="tcp"
            else
                FW_PROTOCOL="$PROTOCOL"
            fi
            ;;
        esac
        if [ -z $FW_PROTOCOL ]; then
            echo "❌ Invalid protocol"
            return
        fi
    fi

    # in/out-interface
    NET_INTERFACES=($(ip a | grep -E "^[0-9]+: " | awk '{print $2}' | sed 's/://g' | grep -v "lo"))
    if [ ${#NET_INTERFACES[*]} -eq 0 ]; then
        echo "❌ No network interfaces found"
        return
    elif [ ${#NET_INTERFACES[*]} -eq 1 ]; then
        FW_IN_INTERFACE="${NET_INTERFACES[0]}"
        FW_OUT_INTERFACE="${NET_INTERFACES[0]}"
    else
        echo "Select network interface:"
        for i in $(seq 1 $(echo $NET_INTERFACES | wc -w)); do
            echo "$i. $(echo $NET_INTERFACES | awk '{print $'$i'}')"
        done
        read -p "Enter in-interface: " IN_INTERFACE
        FW_IN_INTERFACE=$(echo $NET_INTERFACES | awk '{print $'$IN_INTERFACE'}')
        if [ -z $FW_IN_INTERFACE ]; then
            echo "❌ Invalid interface"
            return
        fi

        read -p "Enter out-interface: " OUT_INTERFACE
        FW_OUT_INTERFACE=$(echo $NET_INTERFACES | awk '{print $'$OUT_INTERFACE'}')
        if [ -z $FW_OUT_INTERFACE ]; then
            echo "❌ Invalid interface"
            return
        fi
    fi

    # source ip
    if [ -z $FW_SOURCE_IP_PORT ]; then
        read -p "Enter source <[IP]|*:[PORT]|*> (default: *:*) > " SOURCE_IP_PORT
    else
        SOURCE_IP_PORT=$FW_SOURCE_IP_PORT
    fi
    SOURCE_IP=$(echo "$SOURCE_IP_PORT" | awk -F: '{print $1}' | sed 's/*//g' | sed 's/ //g')
    FW_SOURCE_IP=""
    if [ ! -z $SOURCE_IP ]; then
        FW_SOURCE_IP="--in-interface ${FW_IN_INTERFACE} --source $SOURCE_IP"
    fi
    SOURCE_PORT=$(echo "$SOURCE_IP_PORT" | awk -F: '{print $2}' | sed 's/*//g' | sed 's/ //g')
    FW_SOURCE_PORT=""
    if [ ! -z $SOURCE_PORT ]; then
        FW_SOURCE_PORT="--sport $SOURCE_PORT"
    fi

    # destination ip
    if [ -z $FW_DESTINATION_IP_PORT ]; then
        read -p "Enter destination <[IP]|*:[PORT]|*> (default: *:*) > " DESTINATION_IP_PORT
    else
        DESTINATION_IP_PORT=$FW_DESTINATION_IP_PORT
    fi
    DESTINATION_IP=$(echo "$DESTINATION_IP_PORT" | awk -F: '{print $1}' | sed 's/*//g' | sed 's/ //g')
    FW_DESTINATION_IP=""
    if [ ! -z $DESTINATION_IP ]; then
        FW_DESTINATION_IP="--out-interface ${FW_OUT_INTERFACE} --destination ${DESTINATION_IP}"
    fi
    DESTINATION_PORT=$(echo "$DESTINATION_IP_PORT" | awk -F: '{print $2}' | sed 's/*//g' | sed 's/ //g')
    FW_DESTINATION_PORT=""
    if [ ! -z $DESTINATION_PORT ]; then
        FW_DESTINATION_PORT="--dport $DESTINATION_PORT"
    fi

    # track state
    if [ -z "$FW_STATE" ]; then
        echo "Connection track state <[S]|[CT] - [...STATE]> (Available states: [NEW, ESTABLISHED, RELATED, INVALID]) (default: *): "
        read -p "Enter connection track state: " FW_STATE
    fi
    FW_STATE="$(echo $FW_STATE | sed 's/ //g')"
    if [ ! -z $FW_STATE ] && [[ ! "${FW_STATE}" =~ ^(S|CT)-([A-Z]+(,?))+ ]]; then
        echo "❌ Invalid connection track state format"
        return
    fi
    if [ ! -z $FW_STATE ] && [ "$FW_STATE" != "*" ]; then
        TYPE=$(echo "${FW_STATE}" | awk -F"-" '{print $1}')
        if [ "${TYPE}" == "S" ]; then
            FW_STATE_TYPE_A="state"
            FW_STATE_TYPE_B="state"
        elif [ "${TYPE}" == "CT" ]; then
            FW_STATE_TYPE_A="conntrack"
            FW_STATE_TYPE_B="ctstate"
        else
            echo "❌ Invalid connection track state type"
            return
        fi
        STATES=($(echo $FW_STATE | awk -F"-" '{print $2}'))
        FW_STATE="-m ${FW_STATE_TYPE_A} --${FW_STATE_TYPE_B} ${STATES}"
    else
        FW_STATE=""
    fi

    # rule
    iptables -A ${FW_CHAIN} --proto ${FW_PROTOCOL} ${FW_SOURCE_IP} ${FW_SOURCE_PORT} ${FW_DESTINATION_IP} ${FW_DESTINATION_PORT} ${FW_STATE} -j ${FW_RULE_TYPE}
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"
}
export -f netops_fw_add_rule

netops_fw_delete() {
    FW_CHAIN=$1
    FW_RULE_NUMBER=$2

    # chain
    if [ -z $FW_CHAIN ]; then
        echo "Select chain:"
        echo "1. INPUT"
        echo "2. OUTPUT"
        echo "3. FORWARD"
        read -p "Enter chain: " CHAIN
        case $CHAIN in
        1) FW_CHAIN="INPUT" ;;
        2) FW_CHAIN="OUTPUT" ;;
        3) FW_CHAIN="FORWARD" ;;
        *)
            echo "❌ Invalid chain"
            return
            ;;
        esac
    fi

    # rule number
    if [ -z $FW_RULE_NUMBER ]; then
        read -p "Enter rule number: " RULE_NUMBER
        if [ -z $RULE_NUMBER ]; then
            echo "❌ Invalid rule number"
            return
        fi
        FW_RULE_NUMBER="$RULE_NUMBER"
    fi

    echo -e "\n🔥 Deleting firewall rule"
    iptables -D ${FW_CHAIN} ${FW_RULE_NUMBER}
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"
}
export -f netops_fw_delete

netops_fw_reset() {
    echo -e "\n🔥 Resetting firewall"

    # Set default policy to ACCEPT
    iptables -P INPUT ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -P FORWARD ACCEPT

    # Flush all rules
    iptables -F
    iptables -X
    iptables -t nat -F
    iptables -t nat -X
    iptables -t mangle -F
    iptables -t mangle -X
}
export -f netops_fw_reset

netops_base() {
    echo -e "\n🔥 Setting default firewall policy"
    netops_fw_reset

    # Allow all incoming traffic from localhost
    iptables -I INPUT -i lo -j ACCEPT

    # Allow all incoming ssh traffic
    netops_fw_add_rule "INPUT" "ACCEPT" "tcp" "*" "*:22" "*"

    # Allow all icmp traffic
    iptables -A INPUT -p icmp -j ACCEPT
    iptables -A OUTPUT -p icmp -j ACCEPT

    # Allow all http/https, ssh outgoing traffic
    netops_fw_add_rule "OUTPUT" "ACCEPT" "tcp" "*:22" "*" "*"
    netops_fw_add_rule "OUTPUT" "ACCEPT" "tcp" "*:80" "*" "*"
    netops_fw_add_rule "OUTPUT" "ACCEPT" "tcp" "*:443" "*" "*"

    # Allow all incoming traffic from established connections
    iptables -A INPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT

    # Allow all outgoing traffic from established connections
    iptables -A OUTPUT -m conntrack ! --ctstate INVALID -j ACCEPT

    # Allow DNS resolution traffic
    netops_fw_add_rule "OUTPUT" "ACCEPT" "tcp" "*" "*:53" "*"

    # Block & Log : all outgoing traffic, all incoming traffic, all forwarding traffic
    iptables -N LOGDROP
    iptables -A INPUT -j LOGDROP
    iptables -A OUTPUT -j LOGDROP
    iptables -A FORWARD -j LOGDROP

    iptables -A LOGDROP -j LOG -m limit --limit 10/minute --log-prefix "fw:drop:" --log-level 4
    iptables -A LOGDROP -j DROP

    # Save rules
    netfilter-persistent save

    sed -i 's/NETOPS_BASE_INSTALLED=false/NETOPS_BASE_INSTALLED=true/g' ${FILE_STATUS}
    IS_NETOPS_BASE_INSTALLED=true
}

# --- APP --- #

app_postgresql() {
    echo -e "\n📦 Installing Postgresql"

    sudo apt install -y curl ca-certificates gnupg
    curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/apt.postgresql.org.gpg >/dev/null

    systemctl daemon-reload

    sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    apt install -y postgresql
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    systemctl enable postgresql

    echo -e "\n🔧 Configuring Postgresql"
    local POSTGRES_VERSION_MAJOR=$(psql --version | awk '{print $3}' | awk -F. '{print $1}')
    local POSTGRES_CONF_FILE="/etc/postgresql/${POSTGRES_VERSION_MAJOR}/main/postgresql.conf"
    sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" ${POSTGRES_CONF_FILE}
    sed -i "s/#port = 5432/port = 5432/g" ${POSTGRES_CONF_FILE}
    sed -i "s/#log_destination = 'stderr'/log_destination = 'stderr'/g" ${POSTGRES_CONF_FILE}
    sed -i "s/#log_line_prefix = ''/log_line_prefix = '%t [%p]: [%l-1] user=%u,db=%d,app=%a,client=%h '/g" ${POSTGRES_CONF_FILE}
    sed -i "s/#log_directory = 'log'/log_directory = '/var/log/postgres'/g" ${POSTGRES_CONF_FILE}
    sed -i "s/#log_filename = 'postgresql-%Y-%m-%d_%H%M%S.log'/log_filename = 'postgresql-%Y%m%d.log'/g" ${POSTGRES_CONF_FILE}

    echo -e "\n🔑 Creating postgres super-user"
    local POSTGRES_USER=pgsuperuser
    local POSTGRES_USER_PWD=$(tr -dc 'A-Za-z0-9!#%&' </dev/urandom | head -c 24)
    sudo -u postgres psql -c "CREATE USER ${POSTGRES_USER} WITH SUPERUSER PASSWORD '${POSTGRES_USER_PWD}';"
    echo "\tPostgreSQL SuperAdmin '${POSTGRES_USER}' created with password (keep it secret, you will not be able to see it again) : ${POSTGRES_USER_PWD}"
    read -p "...Press enter to continue"

    IS_FAIL2BAN_INSTALLED=$(test $(apt list --installed | grep fail2ban | wc -l) -gt 0 && echo true || echo false)
    if [ "${IS_FAIL2BAN_INSTALLED}" == "true" ]; then
        echo -e "\n⚙️ Enabling Fail2Ban for PostgreSQL service"
        {
            echo "[postgres-lockuser]"
            echo "enabled = true"
            echo ""
        } >>/etc/fail2ban/jail.d/jail.$(hostname).conf
    fi

    echo -e "\n🔥 Setting Postgresql firewall policy :"
    echo -e "\t-Allow all incoming postgresql traffic"
    netops_fw_add_rule "INPUT" "ACCEPT" "tcp" "*:5432" "*" "*"
    [[ $? == 0 ]] && echo -e "\t\t✅ Done" || echo -e "\t\t❌ Fail"
}
export -f app_postgresql

app_nginx() {
    echo -e "\n📦 Installing Nginx"
    apt install -y nginx
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    echo -e "\n🔥 Setting Nginx firewall policy :"
    echo -e "\t-Allow all incoming http traffic"
    netops_fw_add_rule "INPUT" "ACCEPT" "tcp" "*:80" "*" "*"
    [[ $? == 0 ]] && echo -e "\t\t✅ Done" || echo -e "\t\t❌ Fail"

    echo -e "\t-Allow all incoming https traffic"
    netops_fw_add_rule "INPUT" "ACCEPT" "tcp" "*:443" "*" "*"
    [[ $? == 0 ]] && echo -e "\t\t✅ Done" || echo -e "\t\t❌ Fail"

    echo -e "\n📦 Installing CertBOT"
    apt install -y snapd
    snap install core
    snap refresh core
    sudo snap install --classic certbot
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    sudo ln -s /snap/bin/certbot /usr/bin/certbot
    echo "Use this commande to genarate ssl certificate : sudo certbot certonly --standalone"
}
export -f app_nginx

app_nodejs() {
    echo -e "\n📦 Installing NodeJS"
    apt install -y nodejs npm
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"
}
export -f app_nodejs

app_docker() {
    echo -e "\n📦 Installing Docker"
    apt install -y docker.io
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"
}
export -f app_docker

app_git() {
    echo -e "\n📦 Installing Git"
    apt install -y git
    [[ $? == 0 ]] && echo "✅ Done" || echo "❌ Fail"

    echo -e "\n🔧 Configuring git"
    read -p "Enter git username: " git_username
    read -p "Enter git email: " git_email
    git config --global user.name $hostname
    git config --global user.email $hostname@service.lakaz.co
    ssh-keygen -o -f ~/.ssh/id_rsa -q -N ""
    eval $(ssh-agent -s)
    ssh-add ~/.ssh/id_rsa
    echo "======= COPY THE KEY BELOW ======="
    cat ~/.ssh/id_rsa.pub
    echo "======= COPY THE KEY ABOVE ======="
    echo "SSH key generated, please add it to your git account :"
    echo -e "\t- Title: $hostname"
    echo -e "\t- Key: the key you just copied"
    read -p "Press enter to continue..." choice
    echo "✅ Done"
}
export -f app_git

app_base() {
    echo -e "\n👤 Creating app user : www"
    local APP_USER=www
    local APP_USER_PWD=$(tr -dc 'A-Za-z0-9!#%&' </dev/urandom | head -c 24)
    local APP_HOME_DIR=/opt/app/${APP_USER}
    adduser --home ${APP_HOME_DIR} --shell /bin/bash -p ${APP_USER_PWD} ${APP_USER}
    echo "\tUser '${APP_USER}' created with password (keep it secret, you will not be able to see it again) : ${APP_USER_PWD}"
    read -p "...Press enter to continue"

    echo -e "\n📁 Creating app directories"
    mkdir -p /LOGS && chown -R ${APP_USER}:${APP_USER} /LOGS
    mkdir -p /DATA && chown -R ${APP_USER}:${APP_USER} /DATA
    echo "✅ Done"

    sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' ${APP_HOME_DIR}/.bashrc
    sed -i 's/#export GCC_COLORS/export GCC_COLORS/g' ${APP_HOME_DIR}/.bashrc
    echo "set mouse-=a" >>${APP_HOME_DIR}/.vimrc
    source ${APP_HOME_DIR}/.vimrc
    echo "✅ Done"

    sed -i 's/APP_BASE_INSTALLED=false/APP_BASE_INSTALLED=true/g' ${FILE_STATUS}
    IS_APP_BASE_INSTALLED=true
}

# ----------------------------------------

host_reboot() {
    read -p "WARNING: Reboot is required. Reboot ? [Y/n] > " REBOOT
    if [ "${REBOOT}" != "n" ] && [ "${REBOOT}" != "N" ]; then
        reboot
    fi
}

stop() {
    echo -e "\n🛑 Stopping the script"
    exit $1
}

vps_utils() {
    while true; do
        echo -e "\n🔧 VPS Utils"
        echo "Version: ${VPS_UTILS_VERSION}"
        echo ""
        echo "Witch package should be installed ?\n"

        # SysOPS
        if [ ${IS_SYSOPS_BASE_INSTALLED} == false ]; then
            echo "s0) SysOPS - Base (WARNING: Reboot required)"
        fi
        echo "s1) SysOPS - Add User"
        echo "s2) SysOPS - Patch Managment"

        # NetOPS
        if [ ${IS_NETOPS_BASE_INSTALLED} == false ]; then
            echo "n0) NetOPS - Base (WARNING: Reboot required)"
        fi
        echo "n1) NetOPS - Firewall - List rules"
        echo "n2) NetOPS - Firewall - Add rule"
        echo "n3) NetOPS - Firewall - Delete rule"
        echo "n4) NetOPS - Firewall - Reset rules (WARNING: All rules will be deleted)"

        # App
        if [ ${IS_APP_BASE_INSTALLED} == false ]; then
            echo "a0) App - Base (WARNING: Reboot required)"
        fi
        echo "a1) App - Postgresql"
        echo "a2) App - Nginx"
        echo "a3) App - Nodejs"
        echo "a4) App - Docker"
        echo "a5) App - Git"
        echo "q) Quit"

        choice=
        read -p "> " choice
        case $choice in
        s0)
            if [ $IS_SYSOPS_BASE_INSTALLED == true ]; then
                echo "✅ Already installed"
                continue
            fi
            sysops_base
            host_reboot
            ;;
        s1) sysops_new_user ;;
        s2) sysops_pm ;;
        n0)
            if [ $IS_NETOPS_BASE_INSTALLED == true ]; then
                echo "✅ Already installed"
                continue
            fi
            netops_base
            host_reboot
            ;;
        n1) netops_fw_list_rule ;;
        n2) netops_fw_add_rule ;;
        n3) netops_fw_delete ;;
        n4) netops_fw_reset ;;
        a0)
            if [ $IS_APP_BASE_INSTALLED == true ]; then
                echo "✅ Already installed"
                continue
            fi
            app_base
            ;;
        a1) app_postgresql ;;
        a2) app_nginx ;;
        a3) app_nodejs ;;
        a4) app_docker ;;
        a5) app_git ;;
        *) return ;;
        esac
    done
}
export -f vps_utils
